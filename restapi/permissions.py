from rest_framework.permissions import BasePermission

class isGet(BasePermission):
    def has_permission(self, request, view):
        if request.method == 'GET':
            return True
        return False
    
class isPost(BasePermission):
    def has_permission(self, request, view):
        if request.method == 'POST':
            return True
        return False
    
class isPut(BasePermission):
    def has_permission(self, request, view):
        if request.method == 'PUT':
            return True
        return False
      
class isDelete(BasePermission):
    def has_permission(self, request, view):
        if request.method == 'DELETE':
            return True
        return False
